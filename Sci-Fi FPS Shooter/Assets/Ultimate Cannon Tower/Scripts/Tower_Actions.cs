﻿using UnityEngine;
using System.Collections;
//This script executes commands to change character animations
[RequireComponent (typeof (Animator))]
public class Tower_Actions : MonoBehaviour
{
	public float rotationDamping = 5f;
	public Transform platform;
	private Animator animator;
	private Quaternion rotate_State;
	private bool bool_fire;
 
	void Awake ()
	{
		animator = GetComponent<Animator>();
    }
 
	void LateUpdate()
	{
		if (bool_fire) platform.rotation = rotate_State;
	}

	public void Idle()
	{
		animator.SetBool ("Idle", true);
		bool_fire = false;
	}

	public void Fire()
	{
		rotate_State = platform.rotation;
		bool_fire = true;		
		animator.SetBool ("Fire", true);
 	}

	private void FacePlayer(GameObject Enemy)
	{
		var dir = Enemy.transform.position - transform.position;
		var rotation = Quaternion.LookRotation(dir);
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationDamping);
	}

	private void Update()
	{
		Collider[] colliders = Physics.OverlapSphere(transform.position, 30f);
		foreach (var go in colliders)
			if (go.transform.tag == "Enemy") FacePlayer(go.gameObject);
	}
}
