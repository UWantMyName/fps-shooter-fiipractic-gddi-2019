﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSetup : NetworkBehaviour
{
    [SerializeField] private Behaviour[] componentsToDisable;

    private Camera SceneCamera;

    // Start is called before the first frame update
    void Start()
    {
        if (!isLocalPlayer) // check if the player is controlled by me
        {
            for (int i = 0; i < componentsToDisable.Length; i++)
                componentsToDisable[i].enabled = false;
        }
        else
        {
            SceneCamera = Camera.main;
            if (SceneCamera != null)
            {
                SceneCamera.gameObject.SetActive(false);
            }
        }
    }

    private void OnDisable()
    {
        if (SceneCamera != null)
            SceneCamera.gameObject.SetActive(true);
    }
}
