﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : MonoBehaviour
{
	public int Health = 1000;
	public GameObject DeathScreen;

	public void TakeDamage(int damage)
	{
		Health -= damage;
	}

	IEnumerator Die()
	{
		Time.timeScale = 0;
		// need to instantiate some explosion effects so that the usre knows when he lost
		yield return new WaitForSeconds(4f);
		StopAllCoroutines();
	}

	// Update is called once per frame
	void Update()
    {
        if (Health <= 0)
		{
			StartCoroutine(Die());
		}
    }
}
