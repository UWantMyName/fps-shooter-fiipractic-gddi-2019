﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeScript : MonoBehaviour
{
    [SerializeField] private AudioSource ExplosionSound;
    [SerializeField] private GameObject ExplosionEffect;
    [SerializeField] private GameObject C1;
    [SerializeField] private GameObject C2;
    [SerializeField] private GameObject C3;
    private float delay = 1f;
    [SerializeField] private float radius = 10f;
    private float countdown;
    bool hasExploded = false;

    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0f && !hasExploded)
        {
            hasExploded = true;
            StartCoroutine(Explode());
        }
    }

    IEnumerator Explode()
    {
        ExplosionSound.Play();
        GameObject expl = Instantiate(ExplosionEffect, transform.position, transform.rotation);
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (var g in colliders)
        {
            if (g.transform.tag == "Player") g.GetComponent<PlayerController>().SendMessage("TakeExplosionDamage");
            if (g.transform.tag == "Barrel") g.GetComponent<BarrelScript>().SendMessage("TakeDamage", 80);
            if (g.transform.tag == "Enemy") g.transform.GetComponent<EnemyControllerScript>().SendMessage("TakeExplosionDamage");

            Rigidbody rb = g.GetComponent<Rigidbody>();
            if (rb != null) rb.AddExplosionForce(1000f, transform.position, radius);
        }

        C1.GetComponent<MeshRenderer>().enabled = false;
        C2.GetComponent<MeshRenderer>().enabled = false;
        C3.GetComponent<MeshRenderer>().enabled = false;
        yield return new WaitForSeconds(1.95f);

        Destroy(expl);
        Destroy(gameObject);
    }
}
