﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{
    private bool dead = false;
    private bool canTakeDamage = true;
    [SerializeField] private GunControllerScript Guns;
    [SerializeField] private Text HealthText;
    [SerializeField] private float crouchSpeed = 2f;
    [SerializeField] private float runSpeed = 10f;
    [SerializeField] private float jumpSpeed = 3f;
    [SerializeField] private float movementSpeed = 5f;
    [SerializeField] private float cameraSensitivity = 3f;
    [SerializeField] private Animator GunAnimator;
    [SerializeField] private Animator DamageScreen;
    [SerializeField] private GameObject DeathScreen;

    public bool isCrouching = false;
    public bool isRunning = false;
    public bool isGrounded = true;

    private int health;
    private float GunAnimatorSpeed;
    private PlayerMotor motor;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        motor = GetComponent<PlayerMotor>();
        GunAnimatorSpeed = GunAnimator.speed;
        Spawn();
    }

    public bool IsFullHealth()
    {
        if (health == 100) return true;
        return false;
    }

    private void Spawn()
    {
        Guns.enabled = true;
        // not dead
        dead = false;

        // full health
        health = 100;

        // only handgun equiped
        Guns.Spawn();

        // can take damage
        canTakeDamage = true;

        // so the enemy camera sees the player
        GetComponentInChildren<BoxCollider>().enabled = false;

        // initialize movement
        isCrouching = false;
        isRunning = false;
        isGrounded = true;
    }

    private void TakeDamage(int damage)
    {
        health -= damage;
        StartCoroutine(FlashScreen());
    }

    private void TakeExplosionDamage()
    {
        if (canTakeDamage)
        {
            health -= 80;
            StartCoroutine(FlashScreen());
        }
    }

    private void TakeBarrelExplosionDamage()
    {
        if (canTakeDamage)
        {
            health -= 40;
            StartCoroutine(FlashScreen());
        }
    }

    private void RestoreHealth(int value)
    {
        health += value;
        if (health > 100) health = 100;
    }

    private void OnCollisionEnter(Collision collision)
    {
        isGrounded = true;
    }

    private void DetectMove()
    {
        // calculate our movement velocity as a 3d Vector
        float _xMov = Input.GetAxisRaw("Horizontal");
        float _zMov = Input.GetAxisRaw("Vertical");
        Vector3 _movHorizontal = transform.right * _xMov;
        Vector3 _movVerical = transform.forward * _zMov;

        // Final movement vector
        Vector3 _velocity;
        if (isRunning) _velocity = (_movHorizontal + _movVerical).normalized * runSpeed;
        else if (isCrouching) _velocity = (_movHorizontal + _movVerical).normalized * crouchSpeed;
        else _velocity = (_movHorizontal + _movVerical).normalized * movementSpeed;

        // Apply movement
        motor.Move(_velocity);
    }

    private void DetectJump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            isRunning = false;
            isGrounded = !isGrounded;
            // calculate our jump movement
            float _jumpMov = Input.GetAxisRaw("Jump");
            Vector3 _movUp = transform.up * _jumpMov;

            Vector3 _jumpVelocity = _movUp.normalized * jumpSpeed;

            // Do the jump
            motor.Jump(_jumpVelocity);
        }
    }

    private void DetectRotatePlayer()
    {
        // calculate our rotation as a 3d Vector
        // This is only turns around left and right
        float _yRot = Input.GetAxisRaw("Mouse X");
        Vector3 _rotation = new Vector3(0f, _yRot, 0f) * cameraSensitivity;

        // Apply rotation
        motor.Rotate(_rotation);
    }

    private void DetectRotateCamera()
    {
        // calculate camera rotation as a 3d Vector
        // This rotates the camera up and down
        float _xRot = Input.GetAxisRaw("Mouse Y");
        Vector3 _cameraRotation = new Vector3(_xRot, 0f, 0f) * cameraSensitivity;

        // Apply camera rotation
        motor.RotateCamera(_cameraRotation);
    }

    private void DetectRun()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            isRunning = true;
            isCrouching = false;
            GunAnimator.speed = GunAnimatorSpeed * 2;
        }
        else
        {
            isRunning = false;
            GunAnimator.speed = GunAnimatorSpeed;
        }
    }

    private void DetectCrouch()
    {
        if (Input.GetKey(KeyCode.LeftControl))
        {
            isCrouching = true;
            isRunning = false;
            GunAnimator.speed = GunAnimatorSpeed / 2;
        }
        else
        {
            isCrouching = false;
            GunAnimator.speed = GunAnimatorSpeed;
        }
    }

    private void UpdateHealth()
    {
		HealthText.text = GameObject.Find("Idol").GetComponent<TurretScript>().Health.ToString();
    }

    public void PickSR()
    {
        Guns.SR_pickedUp = true;
        Guns.AddSRAmmo();
    }

    public void PickSMG()
    {
        Guns.SMG_pickedUp = true;
        Guns.AddSMGAmmo();
    }

    public void PickGL()
    {
        Guns.GL_pickedUp = true;
        Guns.AddGLAmmo();
    }

    public void PickLMG()
    {
        Guns.LMG_pickedUp = true;
        Guns.AddLMGAmmo();
    }

    IEnumerator FlashScreen()
    {
        DamageScreen.SetBool("Damaged", true);
        yield return new WaitForSeconds(0.4f);
        DamageScreen.SetBool("Damaged", false);
        StopCoroutine(FlashScreen());
    }

    IEnumerator Die()
    {
        // Disable the player -> can't move, can't rotate, can't shoot and can't take damage

        //can't shoot
        Guns.enabled = false;
        //can't take damage
        canTakeDamage = false;

        GunAnimator.SetBool("Shoot", false);

        // so the camera does not shoot when the player is dead -> he should stop shooting
        GetComponentInChildren<BoxCollider>().enabled = false;

        // Animation for the death screen
        DeathScreen.SetActive(true);
        DeathScreen.GetComponent<Animator>().Play("DeathScreenAnimation");

        // wait 3 seconds
        yield return new WaitForSeconds(3f);

        // disable the death screen
        DeathScreen.SetActive(false);

        // spawn at a new location and stop the coroutine
        Spawn();
        StopCoroutine(Die());
    }

    private void Update()
    {
        if (health <= 0)
        {
            if (!dead)
            {
                dead = true;
                health = 0;
                StartCoroutine(Die());
            }
        }
        else if (health > 0 && !dead)
        {
            if (isGrounded) DetectRun();
            DetectCrouch();
            DetectMove();
            DetectJump();
            DetectRotatePlayer();
            DetectRotateCamera();
        }
        UpdateHealth();
    }
}
