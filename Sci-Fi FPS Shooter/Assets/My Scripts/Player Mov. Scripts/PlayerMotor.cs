﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour
{
    [SerializeField] private Camera cam;

    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private Vector3 cameraRotation = Vector3.zero;

    private Vector3 jump = Vector3.zero;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Gets a movement vector
    public void Move(Vector3 _velocity)
    {
        velocity = _velocity;
    }

    // Gets a rotational vector
    public void Rotate(Vector3 _rotation)
    {
        rotation = _rotation;
    }

    public void Jump(Vector3 _jump)
    {
        jump = _jump;
    }

    // Gets a camera rotational vector
    public void RotateCamera(Vector3 _cameraRotation)
    {
        cameraRotation = _cameraRotation;
    }

    // Run every physics iteration.
    public void FixedUpdate()
    {
        PerformMovement();
        PerformRotation();
        if (jump != Vector3.zero)
        {
            PerformJump();
            jump = Vector3.zero;
        }
    }

    // Perform rotation based on rotation variable
    private void PerformRotation()
    {
        rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
        if (cam != null)
        {
            cam.transform.Rotate(-cameraRotation);
        }
    }

    //Perform movement based on velocity variable;
    private void PerformMovement()
    {
        if (velocity != Vector3.zero)
        {
            rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime); // it also takes physics into consideration. The Translate method does not take care of physics.
        }
    }

    private void PerformJump()
    {
        rb.AddForce(jump, ForceMode.Impulse);
    }
}
