﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControllerScript : MonoBehaviour
{
    private bool dead = false;
    private bool canTakeDamage = true;

    [SerializeField] private Transform spawnPnt1;
    [SerializeField] private Transform spawnPnt2;
    [SerializeField] private Transform spawnPnt3;
    [SerializeField] private Transform spawnPnt4;

    [SerializeField] private EnemyGunScript Guns;
    [SerializeField] private float health;
    [SerializeField] private EnemyAgentScript agentScript;

    private void Start()
    {
        Spawn();
    }

    public bool IsFullHealth()
    {
        if (health == 100) return true;
        return false;
    }

    private void Spawn()
    {
        Guns.enabled = true;
        // not dead
        dead = false;

        // full health
        health = 100;

        // only handgun equiped
        Guns.Spawn();

        // can take damage
        canTakeDamage = true;

        GetComponentInChildren<MeshRenderer>().enabled = true;
        GetComponent<EnemyAgentScript>().enabled = true;
        
        // set the spawn point
        //int x = Random.Range(0, 100);
        //if (x % 4 == 0) transform.position = spawnPnt1.position;
        //else if (x % 4 == 1) transform.position = spawnPnt2.position;
        //else if (x % 4 == 2) transform.position = spawnPnt3.position;
        //else if (x % 4 == 3) transform.position = spawnPnt4.position;
    }

    private void TakeDamage(float value)
    {
        if (canTakeDamage) health -= value;
    }

    private void TakeExplosionDamage()
    {
        if (canTakeDamage) health -= 80;
    }

    private void TakeBarrelExplosionDamage()
    {
        if (canTakeDamage) health -= 40;
    }

    private void RestoreHealth(int value)
    {
        health += value;
        if (health > 100) health = 100;
    }

    public void PickSR()
    {
        Guns.SR_pickedUp = true;
        Guns.AddSRAmmo();
    }

    public void PickSMG()
    {
        Guns.SMG_pickedUp = true;
        Guns.AddSMGAmmo();
    }

    public void PickGL()
    {
        Guns.GL_pickedUp = true;
        Guns.AddGLAmmo();
    }

    public void PickLMG()
    {
        Guns.LMG_pickedUp = true;
        Guns.AddLMGAmmo();
    }

    IEnumerator Die()
    {
        // Disable the player -> can't move, can't rotate, can't shoot and can't take damage

        //can't shoot
        Guns.enabled = false;
        //can't take damage
        canTakeDamage = false;

        // disable the mesh renderer
        GetComponentInChildren<MeshRenderer>().enabled = false;
        GetComponent<EnemyAgentScript>().enabled = false;

        // so the camera does not shoot when the player is dead -> he should stop shooting
        GetComponentInChildren<BoxCollider>().enabled = false;

        // wait 3 seconds
        yield return new WaitForSeconds(3f);

        // spawn at a new location and stop the coroutine
        Spawn();
        StopCoroutine(Die());
    }

    private void Update()
    {
        if (health <= 0)
        {
            if (!dead)
            {
                dead = true;
                health = 0;
                StartCoroutine(Die());
            }
        }
        else if (health > 0 && !dead)
        {
            //// I need to set the destination routes for the nav mesh agent
            //if (agentScript.ISeePlayer)
            //{
            //    int which = Guns.equiped;
            //    Guns.Shoot(which);
            //}
            //else Guns.StopShoot();
        }
    }
}
