﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAgentScript : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private GameObject Player;
    [SerializeField] private Transform LMGSpawnPoint;
    [SerializeField] private Transform SMGSpawnPoint;
    [SerializeField] private Transform GLSpawnPoint;
    [SerializeField] private Transform SRSpawnPoint;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private EnemyGunScript Guns;

    private float[] distances = new float[5];
    public bool isSeekingWeapons = false;
    private Plane[] planes;
    public bool ISeePlayer = false;
    public float maximumDistance = 10f;
    public float rotationDamping = 5f;

    private void DetectPlanes()
    {
        planes = GeometryUtility.CalculateFrustumPlanes(cam);
    }

    private void FacePlayer()
    {
        var dir = Player.transform.position - transform.position;
        var rotation = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationDamping);
    }

    private void FindClosestSpawner()
    {
		float[] dist = new float[4];

		dist[0] = Vector3.Distance(transform.position, SMGSpawnPoint.position);
		dist[1] = Vector3.Distance(transform.position, LMGSpawnPoint.position);
		dist[2] = Vector3.Distance(transform.position, GLSpawnPoint.position);
		dist[3] = Vector3.Distance(transform.position, SRSpawnPoint.position);

		float min = 100000;
		for (int i = 0; i < 4; i++)
			if (dist[i] < min) min = dist[i];

        if (min == dist[1] && !Guns.LMG_pickedUp && LMGSpawnPoint.GetComponentInParent<BoxCollider>().enabled) agent.SetDestination(LMGSpawnPoint.position);
        else if (min == dist[0] && !Guns.SMG_pickedUp && SMGSpawnPoint.GetComponentInParent<BoxCollider>().enabled) agent.SetDestination(SMGSpawnPoint.position);
        else if (min == dist[2] && !Guns.GL_pickedUp && GLSpawnPoint.GetComponentInParent<BoxCollider>().enabled) agent.SetDestination(GLSpawnPoint.position);
        else if (min == dist[3] && !Guns.SR_pickedUp && SRSpawnPoint.GetComponentInParent<BoxCollider>().enabled) agent.SetDestination(SRSpawnPoint.position);
    }

    private void Update()
    {
		if (!isSeekingWeapons && !Guns.SR_pickedUp && !Guns.GL_pickedUp && !Guns.SMG_pickedUp && !Guns.LMG_pickedUp)
		{
			FindClosestSpawner();
			isSeekingWeapons = true;
		}

		//DetectPlanes();
		//if (GeometryUtility.TestPlanesAABB(planes, Player.GetComponentInChildren<BoxCollider>().bounds))
		//{
		//    ISeePlayer = true;
		//    FacePlayer();
		//}
		//else ISeePlayer = false;
	}
}
