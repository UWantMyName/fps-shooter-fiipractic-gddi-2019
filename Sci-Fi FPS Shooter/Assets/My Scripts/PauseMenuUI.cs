﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuUI : MonoBehaviour
{
    [SerializeField] private GameObject Player;
    [SerializeField] private GameObject PauseMenu;
    [SerializeField] private GameObject Gun;
    private bool IsActive;

    private void Start()
    {
        IsActive = PauseMenu.activeInHierarchy;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (IsActive)
            {
                IsActive = false;
                PauseMenu.SetActive(false);
                //Cursor.lockState = CursorLockMode.Locked;
                Player.GetComponent<PlayerMotor>().enabled = true;
                Player.GetComponent<PlayerController>().enabled = true;
                Gun.GetComponent<GunControllerScript>().enabled = true;
                Time.timeScale = 1;
            }
            else
            {
                IsActive = true;
                PauseMenu.SetActive(true);
                //Cursor.lockState = CursorLockMode.None;
                Player.GetComponent<PlayerMotor>().enabled = false;
                Player.GetComponent<PlayerController>().enabled = false;
                Gun.GetComponent<GunControllerScript>().enabled = false;
                Time.timeScale = 0;
            }
        }
    }
}
