﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLSpawner : MonoBehaviour
{
    //[SerializeField] private GameObject Player;
    [SerializeField] private GameObject GL;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            StartCoroutine(StandBy());
            collision.collider.transform.GetComponent<PlayerController>().PickGL();
        }
        if (collision.collider.tag == "Enemy")
        {
            StartCoroutine(StandBy());
            collision.collider.transform.GetComponentInChildren<EnemyControllerScript>().PickGL();
            collision.collider.transform.GetComponent<EnemyAgentScript>().isSeekingWeapons = false;
        }
    }

    IEnumerator StandBy()
    {
        GetComponent<BoxCollider>().enabled = false;
        GL.SetActive(false);
        yield return new WaitForSeconds(15f);
        GetComponent<BoxCollider>().enabled = true;
        GL.SetActive(true);
        StopAllCoroutines();
    }
}
