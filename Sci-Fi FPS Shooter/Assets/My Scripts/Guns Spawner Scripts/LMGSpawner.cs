﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LMGSpawner : MonoBehaviour
{
    [SerializeField] private GameObject LMG;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            StartCoroutine(StandBy());
            collision.collider.transform.GetComponent<PlayerController>().PickLMG();
        }
        if (collision.collider.tag == "Enemy")
        {
            StartCoroutine(StandBy());
            collision.collider.transform.GetComponentInChildren<EnemyControllerScript>().PickLMG();
            collision.collider.transform.GetComponent<EnemyAgentScript>().isSeekingWeapons = false;
        }
    }

    IEnumerator StandBy()
    {
        GetComponent<BoxCollider>().enabled = false;
        LMG.SetActive(false);
        yield return new WaitForSeconds(15f);
        GetComponent<BoxCollider>().enabled = true;
        LMG.SetActive(true);
        StopAllCoroutines();
    }
}
