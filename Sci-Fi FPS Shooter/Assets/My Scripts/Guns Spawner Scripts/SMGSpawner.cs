﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMGSpawner : MonoBehaviour
{
    //[SerializeField] private GameObject Player;
    [SerializeField] private GameObject SMG;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            StartCoroutine(StandBy());
            collision.collider.transform.GetComponent<PlayerController>().PickSMG();
        }
        if (collision.collider.tag == "Enemy")
        {
            StartCoroutine(StandBy());
            collision.collider.transform.GetComponentInChildren<EnemyControllerScript>().PickSMG();
            collision.collider.transform.GetComponent<EnemyAgentScript>().isSeekingWeapons = false;
        }
    }

    IEnumerator StandBy()
    {
        GetComponent<BoxCollider>().enabled = false;
        SMG.SetActive(false);
        yield return new WaitForSeconds(15f);
        GetComponent<BoxCollider>().enabled = true;
        SMG.SetActive(true);
        StopAllCoroutines();
    }
}
