﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperRifleSpawner : MonoBehaviour
{
    //[SerializeField] private GameObject Player;
    [SerializeField] private GameObject SniperRifle;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            StartCoroutine(StandBy());
            collision.collider.transform.GetComponent<PlayerController>().PickSR();
        }
        if (collision.collider.tag == "Enemy")
        {
            StartCoroutine(StandBy());
            collision.collider.transform.GetComponentInChildren<EnemyControllerScript>().PickSR();
            collision.collider.transform.GetComponent<EnemyAgentScript>().isSeekingWeapons = false;
        }
    }

    IEnumerator StandBy()
    {
        GetComponent<BoxCollider>().enabled = false;
        SniperRifle.SetActive(false);
        yield return new WaitForSeconds(15f);
        GetComponent<BoxCollider>().enabled = true;
        SniperRifle.SetActive(true);
        StopAllCoroutines();
    }
}
