﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUpScript : MonoBehaviour
{
    [SerializeField] private int restoreValue = 50;
    [SerializeField] private GameObject c1;
    [SerializeField] private GameObject c2;
    private bool pickedUp = false;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.transform.tag == "Player")
        {
            collision.collider.transform.GetComponent<PlayerController>().SendMessage("RestoreHealth", restoreValue);
            if (!pickedUp)
            {
                pickedUp = true;
                StartCoroutine(PickedUp());
            }
        }
    }

    IEnumerator PickedUp()
    {
        GetComponent<BoxCollider>().enabled = false;
        c1.GetComponent<MeshRenderer>().enabled = false;
        c2.GetComponent<MeshRenderer>().enabled = false;
        yield return new WaitForSeconds(10f);
        pickedUp = false;
        GetComponent<BoxCollider>().enabled = true;
        c1.GetComponent<MeshRenderer>().enabled = true;
        c2.GetComponent<MeshRenderer>().enabled = true;
    }
}
