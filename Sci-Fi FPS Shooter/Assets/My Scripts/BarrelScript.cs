﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelScript : MonoBehaviour
{
    [SerializeField] private GameObject ExplosionEffect;
    private float radius = 10f;
    public float health = 50f;
    private bool canTakeDamage = true;
    private bool dead = false;

    private void TakeDamage(float dmg)
    {
        if (canTakeDamage) health -= dmg;
    }

    private void Update()
    {
        if (health <= 0 && !dead)
        {
            StartCoroutine(Explode());
            dead = true;
            canTakeDamage = false;
        }
    }

    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(10f);
        GetComponent<MeshRenderer>().enabled = true;
        canTakeDamage = true;
        health = 50;
        dead = false;
        StopAllCoroutines();
    }

    IEnumerator Explode()
    {
        GameObject expl = Instantiate(ExplosionEffect, transform.position, transform.rotation);
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        
        foreach (var g in colliders)
        {
            if (g.transform.tag == "Player")
            {
                g.GetComponent<PlayerController>().SendMessage("TakeBarrelExplosionDamage");
            }
            if (g.transform.tag == "Enemy")
            {
                g.GetComponent<EnemyControllerScript>().SendMessage("TakeBarrelExplosionDamage");
            }
        }

        GetComponent<MeshRenderer>().enabled = false;
        yield return new WaitForSeconds(1.95f);
        Destroy(expl);
        StartCoroutine(Respawn());
        StopCoroutine(Explode());
    }
}
