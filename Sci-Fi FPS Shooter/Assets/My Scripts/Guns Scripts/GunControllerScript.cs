﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunControllerScript : MonoBehaviour
{
    private bool cooldown = false;
    private bool startedCooldown = false;

    [SerializeField] Camera FPSCamera;
    [SerializeField] Camera cam;

    [SerializeField] GameObject Handgun;
    private int HcurrentAmmo = 12;

    [SerializeField] GameObject SMG; // Sub-machine gun    
    public bool SMG_pickedUp = false;
    public int SMGcurrentAmmo = 30;
    public int SMGMaxAmmo = 120;
    public int SMGMagAmmo = 30;

    [SerializeField] GameObject LMG; // Light-machine gun
    public bool LMG_pickedUp = false;
    public int LMGcurrentAmmo = 50;
    public int LMGMaxAmmo = 150;
    public int LMGMagAmmo = 50;

    [SerializeField] GameObject GrenadeLauncher;
    public bool GL_pickedUp = false;
    private int GcurrentAmmo = 8;
    private int GMaxAmmo = 16;
    private int GMagAmmo = 8;

    [SerializeField] GameObject SniperRifle;
    public bool SR_pickedUp = false;
    [SerializeField] GameObject SniperScope;
    private int SRcurrentAmmo = 5;
    private int SRMaxAmmo = 20;
    private int SRMagAmmo = 5;

    public int equiped = 1;

    [SerializeField] private Text crntAmmo;
    [SerializeField] private Text mxAmmo;

    [SerializeField] private AudioClip HandgunShootFire;
    [SerializeField] private AudioClip LMGShootFire;
    [SerializeField] private AudioClip SMGShootFire;
    [SerializeField] private AudioClip SRShootFire;

    [SerializeField] private GameObject bulletEffect;

    private bool aim = false;
    private bool isShooting = false;

    private void Start()
    {
        Spawn();
    }

    public void Spawn()
    {
        if (SniperScope.activeInHierarchy)
        {
            SniperScope.SetActive(false);
            FPSCamera.fieldOfView = 60f;
        }

        equiped = 1;
        aim = false;
        SR_pickedUp = false;
        GL_pickedUp = false;
        SMG_pickedUp = false;
        LMG_pickedUp = false;
        isShooting = false;
        HcurrentAmmo = 12;
        SMGcurrentAmmo = 30;
        SMGMaxAmmo = 120;
        LMGcurrentAmmo = 50;
        LMGMaxAmmo = 150;
        GcurrentAmmo = 8;
        GMaxAmmo = 16;
        SRcurrentAmmo = 5;
        SRMaxAmmo = 20;
    }

    private bool CheckIDLE()
    {
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GunIDLE")) return true;
        return false;
    }

    void UnequipAll(int which)
    {
        if (which != 1) Handgun.SetActive(false);
        if (which != 2) SMG.SetActive(false);
        if (which != 3) GrenadeLauncher.SetActive(false);
        if (which != 4) LMG.SetActive(false);
        if (which != 5) SniperRifle.SetActive(false);
    }

    void EquipGun(int which)
    {
        UnequipAll(which);
        if (which == 1 && !Handgun.activeInHierarchy) Handgun.SetActive(true);
        if (which == 2 && !SMG.activeInHierarchy && SMG_pickedUp) SMG.SetActive(true);
        if (which == 3 && !GrenadeLauncher.activeInHierarchy && GL_pickedUp) GrenadeLauncher.SetActive(true);
        if (which == 4 && !LMG.activeInHierarchy && LMG_pickedUp) LMG.SetActive(true);
        if (which == 5 && !SniperRifle.activeInHierarchy && SR_pickedUp) SniperRifle.SetActive(true);
    }

    private void Shoot(int which)
    {
        if (which == 1)
        {
            if (HcurrentAmmo > 0 && GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GunIDLE"))
            {
                GetComponent<AudioSource>().volume = 0.4f;
                GetComponent<AudioSource>().clip = HandgunShootFire;
                GetComponent<AudioSource>().Play();
                Handgun.GetComponent<HandgunScript>().Shoot();
                HcurrentAmmo--;
                if (HcurrentAmmo == 0) HcurrentAmmo = 12;
            }
        }
        if (which == 2 && SMG_pickedUp) SMG.GetComponent<SMGScript>().Shoot();
        if (which == 3 && GL_pickedUp)
        {
            if (GcurrentAmmo > 0 && GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GunIDLE"))
            {
                GrenadeLauncher.GetComponent<GrenadeLauncherScript>().Shoot();
                GcurrentAmmo--;
            }
        }
        if (which == 4 && LMG_pickedUp) LMG.GetComponent<LMGScript>().Shoot();
        if (which == 5 && SR_pickedUp)
        {
            if (SRcurrentAmmo > 0 && GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GunIDLE"))
            {
                GetComponent<AudioSource>().volume = 1f;
                GetComponent<AudioSource>().clip = SRShootFire;
                GetComponent<AudioSource>().Play();
                SniperRifle.GetComponent<SniperRifleScript>().Shoot();
                SRcurrentAmmo--;
            }
        }
    }

    #region Machine Gun Shoot
    public void SMGShoot()
    {
        SMGcurrentAmmo--;
        GetComponent<AudioSource>().volume = 0.1f;
        GetComponent<AudioSource>().clip = SMGShootFire;
        GetComponent<AudioSource>().Play();
        if (SMGcurrentAmmo == 0)
        {
            if (SMGMaxAmmo >= SMGMagAmmo)
            {
                SMGcurrentAmmo = SMGMagAmmo;
                SMGMaxAmmo -= SMGMagAmmo;
                ReloadAuto();
            }
            else if (SMGMaxAmmo > 0 && SMGMaxAmmo <= SMGMagAmmo)
            {
                SMGcurrentAmmo = SMGMaxAmmo;
                SMGMaxAmmo = 0;
                ReloadAuto();
            }
            else if (SMGMaxAmmo == 0)
            {
                // the weapon is not picked up anymore
                SMG_pickedUp = false;
                isShooting = false;
                SMG.GetComponent<SMGScript>().StopShooting();
                equiped = 1;
            }
        }
    }

    public void LMGShoot()
    {
        LMGcurrentAmmo--;
        GetComponent<AudioSource>().volume = 0.3f;
        GetComponent<AudioSource>().clip = LMGShootFire;
        GetComponent<AudioSource>().Play();
        if (LMGcurrentAmmo == 0)
        {
            GetComponent<AudioSource>().clip = null;
            if (LMGMaxAmmo >= LMGMagAmmo)
            {
                LMGcurrentAmmo = LMGMagAmmo;
                LMGMaxAmmo -= LMGMagAmmo;
                ReloadAuto();
            }
            else if (LMGMaxAmmo > 0 && LMGMaxAmmo <= LMGMagAmmo)
            {
                LMGcurrentAmmo = LMGMaxAmmo;
                LMGMaxAmmo = 0;
                ReloadAuto();
            }
            else if (LMGMaxAmmo == 0)
            {
                // the weapon is not picked up anymore
                LMG_pickedUp = false;
                isShooting = false;
                LMG.GetComponent<LMGScript>().StopShooting();
                equiped = 1;
            }
        }
    }

    private void ShootMachineGun(int which)
    {
        GetComponent<Animator>().SetInteger("Equiped", which);
        GetComponent<Animator>().SetBool("Shoot", true);
    }

    public void DisableShoot()
    {
        GetComponent<Animator>().SetBool("Shoot", false);
        GetComponent<Animator>().SetInteger("Equiped", 0);
    }
    #endregion

    #region Aim Sniper Rifle
    private void EnableScope()
    {
        SniperScope.SetActive(true);
        FPSCamera.fieldOfView = 15;
    }

    private void DisableScope()
    {
        SniperScope.SetActive(false);
        FPSCamera.fieldOfView = 60;
    }

    private void Aim()
    {
        if (!aim)
        {
            SniperRifle.GetComponent<SniperRifleScript>().Aim();
            aim = true;
        }
        else
        {
            SniperRifle.GetComponent<SniperRifleScript>().UnAim();
            aim = false;
        }

    }
    #endregion

    #region Reload Actions
    private void ReloadPressed()
    {
        if (equiped == 2 && SMGcurrentAmmo != SMGMagAmmo)
        {
            if (SMGMagAmmo - SMGcurrentAmmo > SMGMaxAmmo && SMGMaxAmmo > 0)
            {
                SMGcurrentAmmo += SMGMaxAmmo;
                SMGMaxAmmo = 0;
                Reload();
            }
            else if (SMGMagAmmo - SMGcurrentAmmo <= SMGMaxAmmo)
            {
                SMGMaxAmmo -= SMGMagAmmo;
                SMGMaxAmmo += SMGcurrentAmmo;
                SMGcurrentAmmo = SMGMagAmmo;
                Reload();
            }
        }
        else if (equiped == 3 && GcurrentAmmo != GMagAmmo)
        {
            if (GMagAmmo - GcurrentAmmo > GMaxAmmo && GMaxAmmo > 0)
            {
                GcurrentAmmo += GMaxAmmo;
                GMaxAmmo = 0;
                Reload();
            }
            else if (GMagAmmo - GcurrentAmmo <= GMaxAmmo)
            {
                GMaxAmmo -= GMagAmmo;
                GMaxAmmo += GcurrentAmmo;
                GcurrentAmmo = GMagAmmo;
                Reload();
            }
        }
        else if (equiped == 4 && LMGcurrentAmmo != LMGMagAmmo)
        {
            if (LMGMagAmmo - LMGcurrentAmmo > LMGMaxAmmo && LMGMaxAmmo > 0)
            {
                LMGcurrentAmmo += LMGMaxAmmo;
                LMGMaxAmmo = 0;
                Reload();
            }
            else if (LMGMagAmmo - LMGcurrentAmmo <= LMGMaxAmmo)
            {
                LMGMaxAmmo -= LMGMagAmmo;
                LMGMaxAmmo += LMGcurrentAmmo;
                LMGcurrentAmmo = LMGMagAmmo;
                Reload();
            }
        }
        else if (equiped == 5 && SRcurrentAmmo != SRMagAmmo)
        {
            if (SRMagAmmo - SRcurrentAmmo > SRMaxAmmo && SRMaxAmmo > 0)
            {
                SRcurrentAmmo += SRMaxAmmo;
                SRMaxAmmo = 0;
                Reload();
            }
            else if (SRMagAmmo - SRcurrentAmmo <= SRMaxAmmo)
            {
                SRMaxAmmo -= SRMagAmmo;
                SRMaxAmmo += SRcurrentAmmo;
                SRcurrentAmmo = SRMagAmmo;
                Reload();
            }
        }
    }

    private void Reload()
    {
        GetComponent<Animator>().SetBool("Reload", true);
    }

    private void ReloadAuto()
    {
        GetComponent<Animator>().SetBool("Reload", true);
        GetComponent<Animator>().SetInteger("Equiped", 0);
    }

    private void ReloadWhileAim()
    {
        DisableScope();
        GetComponent<Animator>().SetBool("Reload", true);
        GetComponent<Animator>().SetInteger("Equiped", 5);
    }

    private void UnReload()
    {
        GetComponent<Animator>().SetBool("Reload", false);
    }

    private void CheckReload() // only check for guns 1,3 and 5
    {
        if (equiped == 1 && HcurrentAmmo == 0)
        {
            Reload();
            HcurrentAmmo = 12;
        }
        if (equiped == 3 && GcurrentAmmo == 0)
        {
            if (GMaxAmmo >= GMagAmmo)
            {
                GcurrentAmmo = GMagAmmo;
                GMaxAmmo -= GMagAmmo;
                Reload();
            }
            else if (GMaxAmmo > 0 && GMaxAmmo <= GMagAmmo)
            {
                GcurrentAmmo = GMaxAmmo;
                GMaxAmmo = 0;
                Reload();
            }
            else if (GMaxAmmo == 0)
            {
                // the weapon is not picked up anymore
                GL_pickedUp = false;
                equiped = 1;
            }
        }
        if (equiped == 5 && SRcurrentAmmo == 0)
        {
            if (SRMaxAmmo >= SRMagAmmo)
            {
                SRcurrentAmmo = SRMagAmmo;
                SRMaxAmmo -= SRMagAmmo;
                Reload();
            }
            else if (SRMaxAmmo > 0 && SRMaxAmmo <= SRMagAmmo)
            {
                SRcurrentAmmo = SRMaxAmmo;
                SRMaxAmmo = 0;
                Reload();
            }
            else if (SRMaxAmmo == 0)
            {
                // the weapon is not picked up anymore
                SR_pickedUp = false;
                equiped = 1;
            }
        }
    }
    #endregion

    private void UpdateAmmo()
    {
        if (equiped == 1)
        {
            crntAmmo.text = HcurrentAmmo.ToString();
            mxAmmo.text = " ";
        }
        else if (equiped == 2 && SMG_pickedUp)
        {
            crntAmmo.text = ((int)SMGcurrentAmmo).ToString();
            mxAmmo.text = "/" + SMGMaxAmmo.ToString();
        }
        else if (equiped == 3 && GL_pickedUp)
        {
            crntAmmo.text = GcurrentAmmo.ToString();
            mxAmmo.text = "/" + GMaxAmmo.ToString();
        }
        else if (equiped == 4 && LMG_pickedUp)
        {
            crntAmmo.text = ((int)LMGcurrentAmmo).ToString();
            mxAmmo.text = "/" + LMGMaxAmmo.ToString();
        }
        else if (equiped == 5 && SR_pickedUp)
        {
            crntAmmo.text = SRcurrentAmmo.ToString();
            mxAmmo.text = "/" + SRMaxAmmo.ToString();
        }
    }

    public void AddSMGAmmo()
    {
        SMGMaxAmmo += 2 * SMGMagAmmo;
        if (SMGMaxAmmo >= 200) SMGMaxAmmo = 200;
    }

    public void AddGLAmmo()
    {
        GMaxAmmo += GMagAmmo;
        if (GMaxAmmo >= 16) GMaxAmmo = 16;
    }

    public void AddLMGAmmo()
    {
        LMGMaxAmmo += LMGMagAmmo;
        if (LMGMaxAmmo >= 200) LMGMaxAmmo = 200;
    }

    public void AddSRAmmo()
    {
        SRMaxAmmo += 2 * SRMagAmmo;
        if (SRMaxAmmo >= 20) SRMaxAmmo = 20;
    }

    IEnumerator WaitForAim()
    {
        yield return new WaitForSeconds(0.5f);
        startedCooldown = false;
        cooldown = false;
        StopCoroutine(WaitForAim());
    }

    private void Update()
    {
        // detect normal shooting without aim
        if (Input.GetMouseButtonDown(0) && equiped % 2 != 0 && GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GunIDLE")) Shoot(equiped);

        // detect shooting while aiming with sniper rifle
        if (Input.GetMouseButtonDown(0) && equiped == 5 && aim && SniperScope.activeInHierarchy && !cooldown)
        {
            cooldown = true;
            if (!startedCooldown)
            {
                startedCooldown = true;
                StartCoroutine(WaitForAim());
            }
            GetComponent<AudioSource>().volume = 1f;
            GetComponent<AudioSource>().clip = SRShootFire;
            GetComponent<AudioSource>().Play();

            RaycastHit hit;
            if (Physics.Raycast(FPSCamera.transform.position, FPSCamera.transform.forward, out hit, SniperRifle.GetComponent<SniperRifleScript>().range))
            {
                if (hit.transform.tag == "Barrel") hit.transform.GetComponent<BarrelScript>().SendMessage("TakeDamage", SniperRifle.GetComponent<SniperRifleScript>().damage);
                if (hit.transform.tag == "Enemy") hit.transform.GetComponent<RobotScript>().SendMessage("TakeDamage", SniperRifle.GetComponent<SniperRifleScript>().damage);

                GameObject impactGO = Instantiate(bulletEffect, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impactGO, 0.3f);
            }

            SRcurrentAmmo--;
            if (SRcurrentAmmo == 0)
            {
                if (SRMaxAmmo >= SRMagAmmo)
                {
                    SRcurrentAmmo = SRMagAmmo;
                    SRMaxAmmo -= SRMagAmmo;
                    ReloadWhileAim();
                }
                else if (SRMaxAmmo > 0 && SRMaxAmmo <= SRMagAmmo)
                {
                    SRcurrentAmmo = SRMaxAmmo;
                    SRMaxAmmo = 0;
                    ReloadWhileAim();
                }
                else if (SRMaxAmmo == 0)
                {
                    // the weapon is not picked up anymore
                    SR_pickedUp = false;
                    DisableScope();
                    aim = false;
                    equiped = 1;
                }
            }
        }

        // detect automatic shooting
        if (Input.GetMouseButton(0) && equiped % 2 == 0)
        {
            Shoot(equiped);
        }
        else
        {
            if (equiped == 2) SMG.GetComponent<SMGScript>().StopShooting();
            if (equiped == 4) LMG.GetComponent<LMGScript>().StopShooting();
        }
        
        // detect aim
        if (Input.GetMouseButtonDown(1) && equiped == 5) Aim();

        // detect other actions, while NOT AIMING
        if (!aim && CheckIDLE())
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                ReloadPressed();
                UpdateAmmo();
            }

            if (Input.GetKeyDown(KeyCode.Alpha1)) equiped = 1;
            if (Input.GetKeyDown(KeyCode.Alpha2) && SMG_pickedUp) equiped = 2;
            if (Input.GetKeyDown(KeyCode.Alpha3) && GL_pickedUp) equiped = 3;
            if (Input.GetKeyDown(KeyCode.Alpha4) && LMG_pickedUp) equiped = 4;
            if (Input.GetKeyDown(KeyCode.Alpha5) && SR_pickedUp) equiped = 5;

            if (Input.GetAxisRaw("Mouse ScrollWheel") < 0)
            {
                equiped++;
                if (equiped == 2 && !SMG_pickedUp) equiped++;
                if (equiped == 3 && !GL_pickedUp) equiped++;
                if (equiped == 4 && !LMG_pickedUp) equiped++;
                if (equiped == 5 && !SR_pickedUp) equiped++;
                if (equiped > 5) equiped -= 5;
            }
            else if (Input.GetAxisRaw("Mouse ScrollWheel") > 0)
            {
                equiped--;
                if (equiped <= 0) equiped += 5;
                if (equiped == 2 && !SMG_pickedUp) equiped--;
                if (equiped == 3 && !GL_pickedUp) equiped--;
                if (equiped == 4 && !LMG_pickedUp) equiped--;
                if (equiped == 5 && !SR_pickedUp) equiped--;
            }
        }

        if (equiped == 1) EquipGun(equiped);
        if (equiped == 2 && SMG_pickedUp) EquipGun(equiped);
        if (equiped == 3 && GL_pickedUp) EquipGun(equiped);
        if (equiped == 4 && LMG_pickedUp) EquipGun(equiped);
        if (equiped == 5 && SR_pickedUp) EquipGun(equiped);

        CheckReload();
        UpdateAmmo();
    }
}
