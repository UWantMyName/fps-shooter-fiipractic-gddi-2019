﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMGScript : MonoBehaviour
{
    [SerializeField] private Camera FPSCamera;
    [SerializeField] private GameObject Gun;
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] private float damage;
    [SerializeField] private float range;

    [SerializeField] private GameObject bulletEffect;

    public void StopShooting()
    {
        Gun.GetComponent<Animator>().SetBool("Shoot", false);
        Gun.GetComponent<Animator>().SetInteger("Equiped", 2);
    }

    public void Shoot()
    {
        if (Gun.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GunIDLE"))
        {
            // set the animation of the gun
            Gun.GetComponent<Animator>().SetBool("Shoot", true);
            Gun.GetComponent<Animator>().SetInteger("Equiped", 2);
        }

        if (Gun.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("SMGShoot"))
        {
            // shoot a raycast for the action of the shooting
            //Gun.GetComponent<GunControllerScript>().SMGShoot();
            
            muzzleFlash.Play();
            RaycastHit hit;
            if (Physics.Raycast(FPSCamera.transform.position, FPSCamera.transform.forward, out hit, range))
            {
                if (hit.transform.tag == "Barrel") hit.transform.GetComponent<BarrelScript>().SendMessage("TakeDamage", damage);
                if (hit.transform.tag == "Enemy") hit.transform.GetComponent<RobotScript>().SendMessage("TakeDamage", damage);

                GameObject impactGO = Instantiate(bulletEffect, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impactGO, 0.3f);
            }
        }
    }
}
