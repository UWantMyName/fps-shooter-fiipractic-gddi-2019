﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeLauncherScript : MonoBehaviour
{
    [SerializeField] private float throwForce;
    [SerializeField] private GameObject grenade;
    [SerializeField] private Transform gunEnd;
    [SerializeField] private GameObject Gun;
    [SerializeField] private GameObject Player;

    public void Shoot()
    {
        if (Gun.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GunIDLE"))
        {
            Gun.GetComponent<Animator>().SetBool("Shoot", true);
            Gun.GetComponent<Animator>().SetInteger("Equiped", 3);

            GameObject grnd = Instantiate(grenade, gunEnd.position, gunEnd.rotation);
            Rigidbody rb = grnd.GetComponent<Rigidbody>();

            rb.AddForce((Gun.transform.forward / 2 + 2 * Player.transform.forward) * throwForce);
        }
    }
}
