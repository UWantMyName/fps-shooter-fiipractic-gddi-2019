﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandgunScript : MonoBehaviour
{
    [SerializeField] private Camera FPSCamera;
    [SerializeField] private GameObject Gun;
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] private float damage;
    [SerializeField] private float range;

    [SerializeField] private GameObject bulletEffect;

    public void Shoot()
    {
        if (Gun.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GunIDLE"))
        {
            muzzleFlash.Play();
            // set the animation of the gun
            Gun.GetComponent<Animator>().SetBool("Shoot", true);
            Gun.GetComponent<Animator>().SetInteger("Equiped", 1);

            // shoot a raycast for the action of the shooting
            RaycastHit hit;
            if (Physics.Raycast(FPSCamera.transform.position, FPSCamera.transform.forward, out hit, range))
            {
                if (hit.transform.tag == "Barrel") hit.transform.GetComponent<BarrelScript>().SendMessage("TakeDamage", damage);
                if (hit.transform.tag == "Enemy") hit.transform.GetComponent<RobotScript>().SendMessage("TakeDamage", damage);

                GameObject impactGO = Instantiate(bulletEffect, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impactGO, 0.3f);
            }
        }
    }

    public void EnemyShoot()
    {
        if (Gun.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("EnemyGunIDLE"))
        {
            muzzleFlash.Play();
            // set the animation of the gun
            Gun.GetComponent<Animator>().SetBool("Shoot", true);
            Gun.GetComponent<Animator>().SetBool("Handgun", true);

            RaycastHit hit;
            if (Physics.Raycast(FPSCamera.transform.position, FPSCamera.transform.forward, out hit, range))
            {
                //if (hit.transform.tag == "Barrel") hit.transform.GetComponent<BarrelScript>().SendMessage("TakeDamage", damage);
                if (hit.transform.tag == "Player")
                {
                    hit.transform.GetComponent<PlayerController>().SendMessage("TakeDamage", damage);
                }
            }
        }
    }
}
