﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperRifleScript : MonoBehaviour
{
    [SerializeField] private GameObject Crosshair;
    [SerializeField] private Camera FPSCamera;
    [SerializeField] private GameObject Gun;
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] public float damage;
    [SerializeField] public float range;

    [SerializeField] private GameObject bulletEffect;

    public void UnAim()
    {
        if (Gun.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("SniperRifleAim"))
        {
            Gun.GetComponent<Animator>().SetInteger("Equiped", 5);
            Gun.GetComponent<Animator>().SetBool("Aim", false);
            Crosshair.SetActive(true);
        }
    }

    public void Aim()
    {
        if (Gun.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GunIDLE"))
        {
            Gun.GetComponent<Animator>().SetBool("Aim", true);
            Gun.GetComponent<Animator>().SetInteger("Equiped", 5);
            Crosshair.SetActive(false);
        }
    }

    public void Shoot()
    {
        if (Gun.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GunIDLE"))
        {
            muzzleFlash.Play();
            // set the animation of the gun
            Gun.GetComponent<Animator>().SetBool("Shoot", true);
            Gun.GetComponent<Animator>().SetInteger("Equiped", 5);
        }            

        // shoot a raycast for the action of the shooting
        RaycastHit hit;
        if (Physics.Raycast(FPSCamera.transform.position, FPSCamera.transform.forward, out hit, range))
        {
            if (hit.transform.tag == "Barrel") hit.transform.GetComponent<BarrelScript>().SendMessage("TakeDamage", damage);
            if (hit.transform.tag == "Enemy") hit.transform.GetComponent<RobotScript>().SendMessage("TakeDamage", damage);

            GameObject impactGO = Instantiate(bulletEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impactGO, 0.3f);
        }
    }
}
