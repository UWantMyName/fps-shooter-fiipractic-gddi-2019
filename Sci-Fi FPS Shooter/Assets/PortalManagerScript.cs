﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortalManagerScript : MonoBehaviour
{
	[SerializeField] private GameObject Portal1;
	[SerializeField] private GameObject Portal2;
	[SerializeField] private GameObject Portal3;
	[SerializeField] private GameObject Portal4;
	[SerializeField] private Text SucceedText;

	private bool started_Level1 = false;

	private int Level;

	private void Start()
	{
		Level = 1;
	}

	public void DisablePortal(string NameOfPortal)
	{
		GameObject.Find(NameOfPortal).GetComponent<PortalScript>().canSpawn = false;
	}

	private void Level1()
	{
		Portal1.GetComponent<PortalScript>().MaxNrRobots = 5;
		Portal2.GetComponent<PortalScript>().MaxNrRobots = 0;
		Portal2.GetComponent<PortalScript>().canSpawn = false;
		Portal3.GetComponent<PortalScript>().MaxNrRobots = 0;
		Portal3.GetComponent<PortalScript>().canSpawn = false;
		Portal4.GetComponent<PortalScript>().MaxNrRobots = 0;
		Portal4.GetComponent<PortalScript>().canSpawn = false;
	}

	private void CheckSuccess()
	{
		bool startedAnim = false;
		int x, y, z, t;
		int a, b, c, d;
		x = Portal1.GetComponent<PortalScript>().MaxNrRobots;
		y = Portal2.GetComponent<PortalScript>().MaxNrRobots;
		z = Portal3.GetComponent<PortalScript>().MaxNrRobots;
		t = Portal4.GetComponent<PortalScript>().MaxNrRobots;

		a = Portal1.GetComponent<PortalScript>().NrRobots;
		b = Portal2.GetComponent<PortalScript>().NrRobots;
		c = Portal3.GetComponent<PortalScript>().NrRobots;
		d = Portal4.GetComponent<PortalScript>().NrRobots;

		if (x == a && y == b && z == c && t == d && !startedAnim)
		{
			startedAnim = true;
			StartCoroutine(SucceedAnimation());
		}
	}
	
	private IEnumerator SucceedAnimation()
	{
		SucceedText.enabled = true;
		SucceedText.text = "Level is clear!";
		yield return new WaitForSeconds(5f);
		SucceedText.text = "Get ready in 10...";
		yield return new WaitForSeconds(1f);
		SucceedText.text = "Get ready in 9...";
		yield return new WaitForSeconds(1f);
		SucceedText.text = "Get ready in 8...";
		yield return new WaitForSeconds(1f);
		SucceedText.text = "Get ready in 7...";
		yield return new WaitForSeconds(1f);
		SucceedText.text = "Get ready in 6...";
		yield return new WaitForSeconds(1f);
		SucceedText.text = "Get ready in 5...";
		yield return new WaitForSeconds(1f);
		SucceedText.text = "Get ready in 4...";
		yield return new WaitForSeconds(1f);
		SucceedText.text = "Get ready in 3...";
		yield return new WaitForSeconds(1f);
		SucceedText.text = "Get ready in 2...";
		yield return new WaitForSeconds(1f);
		SucceedText.text = "Get ready in 1...";
		yield return new WaitForSeconds(1f);
		SucceedText.text = "Attack!";
		SucceedText.enabled = false;
		StopAllCoroutines();
	}

	private void Update()
	{
		if (Level == 1 && !started_Level1)
		{
			started_Level1 = true;
			Level1();
		}
		CheckSuccess();
	}
}
