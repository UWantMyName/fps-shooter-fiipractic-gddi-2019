﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalScript : MonoBehaviour
{
	[SerializeField] private GameObject Player;
	[SerializeField] private GameObject Robot1;
	[SerializeField] private GameObject Robot2;
	[SerializeField] private GameObject SpawnPosition;

	public bool canSpawn = true;

	private int Health = 10000;
	public int MaxNrRobots;
	public int NrRobots = 0;
	public int SpawnRate;

	public void TakeDamage(int damage)
	{
		Health -= damage;
	}
	
	private void Update()
	{
		if (NrRobots == MaxNrRobots)
		{
			// Level is complete
			GameObject.Find("PORTAL MANAGER").GetComponent<PortalManagerScript>().DisablePortal(gameObject.name);
		}
		if (Time.timeScale == 1 && canSpawn && NrRobots < MaxNrRobots) SpawnRate++;
		if (SpawnRate > 500)
		{
			SpawnRate = 0;
			int x = Random.Range(0, 10);
			if (x % 2 == 0) Instantiate(Robot1, SpawnPosition.transform.position, transform.rotation);
			else if (x % 2 == 1) Instantiate(Robot2, SpawnPosition.transform.position, transform.rotation);
			NrRobots++;
		}
	}

}
