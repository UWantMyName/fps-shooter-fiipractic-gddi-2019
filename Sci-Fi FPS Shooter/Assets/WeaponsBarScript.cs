﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponsBarScript : MonoBehaviour
{
    [SerializeField] private GameObject W1;
    [SerializeField] private GameObject Weapon1;
    [SerializeField] private GameObject W2;
    [SerializeField] private GameObject Weapon2;
    [SerializeField] private GameObject W3;
    [SerializeField] private GameObject Weapon3;
    [SerializeField] private GameObject W4;
    [SerializeField] private GameObject Weapon4;
    [SerializeField] private GameObject W5;
    [SerializeField] private GameObject Weapon5;

    [SerializeField] private Sprite NormalTile;
    [SerializeField] private Sprite HighlightedTile;

    [SerializeField] private GunControllerScript Guns;

    private void Update()
    {
        if (Guns.SMG_pickedUp)
        {
            W2.GetComponent<Image>().enabled = true;
            Weapon2.GetComponent<Image>().enabled = true;
        }
        else
        {
            W2.GetComponent<Image>().enabled = false;
            Weapon2.GetComponent<Image>().enabled = false;
        }

        if (Guns.GL_pickedUp)
        {
            W3.GetComponent<Image>().enabled = true;
            Weapon3.GetComponent<Image>().enabled = true;
        }
        else
        {
            W3.GetComponent<Image>().enabled = false;
            Weapon3.GetComponent<Image>().enabled = false;
        }

        if (Guns.LMG_pickedUp)
        {
            W4.GetComponent<Image>().enabled = true;
            Weapon4.GetComponent<Image>().enabled = true;
        }
        else
        {
            W4.GetComponent<Image>().enabled = false;
            Weapon4.GetComponent<Image>().enabled = false;
        }

        if (Guns.SR_pickedUp)
        {
            W5.GetComponent<Image>().enabled = true;
            Weapon5.GetComponent<Image>().enabled = true;
        }
        else
        {
            W5.GetComponent<Image>().enabled = false;
            Weapon5.GetComponent<Image>().enabled = false;
        }

        if (Guns.equiped == 1) W1.GetComponent<Image>().sprite = HighlightedTile;
        else W1.GetComponent<Image>().sprite = NormalTile;

        if (Guns.equiped == 2 && Guns.SMG_pickedUp) W2.GetComponent<Image>().sprite = HighlightedTile;
        else W2.GetComponent<Image>().sprite = NormalTile;

        if (Guns.equiped == 3 && Guns.GL_pickedUp) W3.GetComponent<Image>().sprite = HighlightedTile;
        else W3.GetComponent<Image>().sprite = NormalTile;

        if (Guns.equiped == 4 && Guns.LMG_pickedUp) W4.GetComponent<Image>().sprite = HighlightedTile;
        else W4.GetComponent<Image>().sprite = NormalTile;

        if (Guns.equiped == 5 && Guns.SR_pickedUp) W5.GetComponent<Image>().sprite = HighlightedTile;
        else W5.GetComponent<Image>().sprite = NormalTile;
    }
}
