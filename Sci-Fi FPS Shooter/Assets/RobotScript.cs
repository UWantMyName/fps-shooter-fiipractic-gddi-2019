﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RobotScript : MonoBehaviour
{
	public int Health;
	public NavMeshAgent Agent;
	public GameObject Idol;
	public GameObject PlayerModel;

	public GameObject ExplosionEffect;

	private bool dead = false;

    void Start()
    {
		Agent = GetComponent<NavMeshAgent>();
		Idol = GameObject.Find("Idol");
		Agent.SetDestination(Idol.transform.position);
	}
	
	public void TakeDamage(int damage)
	{
		Health -= damage;
	}

	private IEnumerator Die()
	{
		GameObject go = Instantiate(ExplosionEffect, transform.position, transform.rotation);
		PlayerModel.SetActive(false);
		yield return new WaitForSeconds(2);
		Destroy(go);
		Destroy(gameObject);
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.name == "Idol")
		{
			collision.collider.gameObject.GetComponent<TurretScript>().TakeDamage(40);
			if (!dead)
			{
				dead = true;
				StartCoroutine(Die());
			}
		}
	}

	private void Update()
	{
		if (Health <= 0) Destroy(gameObject);
	}
}
